import fs from 'node:fs'
import {fileTypeFromFile} from 'file-type';

const CACHE_TTL = 5*1000

const Statics = {
	cacheBytes: {},
	cacheMime: {},
	cacheExt: {},
	cacheFormat: {},
}

export const FileTypeResolver = {

	checkBytes: async function(filepath,searchContent,searchLimit=64,searchBackward=false) {

		let key = JSON.stringify([filepath,searchContent,searchLimit,searchBackward]);
		if(Statics.cacheBytes[key] && Statics.cacheBytes[key]?.t > Date.now())
		{
			return Statics.cacheBytes[key].v;
		}

		let v = await new Promise((resolve, reject) => {

			if(!fs.existsSync(filepath))
				resolve(false)

			var stream = null;
			if(searchBackward)
			{
				let stats = fs.statSync(filepath)
				if(stats && stats?.size > 0)
				{
					stream = fs.createReadStream(filepath,{start:Math.max(0,stats.size-searchLimit),end:stats.size});
				}
				else
				{
					reject();
				}
			}
			else
			{
				stream = fs.createReadStream(filepath,{start:0,end:searchLimit});
			}

			var content = '';

			stream.on('data',function(d){ content += d.toString(); });

			stream.on('error',err => {
				reject(err);
			});

			stream.on('close',err => {
				let found = content.match(searchContent) ? true:false;
				resolve(found)
			});
		});

		Statics.cacheBytes[key] = { v, t:Date.now()+CACHE_TTL };
		return v;
	},

	checkMime: async function(filepath,mimetypes) {
		mimetypes = typeof mimetypes === 'string' ? [mimetypes] : mimetypes
		mimetypes = mimetypes?.length > 0 ? mimetypes : [];
		let key = JSON.stringify([filepath,mimetypes]);

		if(Statics.cacheMime[key] && Statics.cacheMime[key]?.t > Date.now())
		{
			return Statics.cacheMime[key].v;
		}

		let v = await this.getFormat(filepath); // { ext: 'jpg', mime: 'image/jpeg' }
		v = v && mimetypes.indexOf(v?.mime)>=0 ? true:false;

		Statics.cacheMime[key] = { v, t:Date.now()+CACHE_TTL };
		return v;
	},

	checkExt: async function(filepath,exts) {
		exts = typeof exts === 'string' ? [exts] : exts
		exts = exts?.length > 0 ? exts : [];
		let key = JSON.stringify([filepath,exts]);

		if(Statics.cacheExt[key] && Statics.cacheExt[key]?.t > Date.now())
		{
			return Statics.cacheExt[key].v;
		}

		let v = await this.getFormat(filepath); // { ext: 'jpg', mime: 'image/jpeg' }
		v = v && exts.indexOf(v?.ext)>=0 ? true:false;

		Statics.cacheExt[key] = { v, t:Date.now()+CACHE_TTL };
		return v;
	},

	getFormat: async function(filepath) {
		var key = filepath
		if(Statics.cacheFormat[key] && Statics.cacheFormat[key]?.t > Date.now())
		{
			return Statics.cacheFormat[key].v;
		}

		let v = null;
		v = await fileTypeFromFile(filepath); // { ext: 'jpg', mime: 'image/jpeg' }

		// additionnal test for weird filetype detection
		if((v===undefined || v.ext==='xml') && await this.builtin.isSVG(filepath))
			v = { ext: 'svg', mime: 'image/svg+xml' }
		else if(v.ext==='cur' && await this.builtin.isTGA(filepath))
			v = { ext: 'tga', mime: 'image/tga' }

		Statics.cacheFormat[key] = { v, t:Date.now()+CACHE_TTL };
		return v;
	},

	builtin: {
		isAI:      function(filepath) { return FileTypeResolver.checkExt(filepath, 'ai') },
		isBMP:     function(filepath) { return FileTypeResolver.checkMime(filepath,'image/bmp') },
		isEPS:     function(filepath) { return FileTypeResolver.checkMime(filepath,'application/eps') },
		isGIF:     function(filepath) { return FileTypeResolver.checkMime(filepath,'image/gif') },
		isHEIC:    function(filepath) { return FileTypeResolver.checkMime(filepath,'image/heic') },
		isICO:     function(filepath) { return FileTypeResolver.checkBytes(filepath, '\x00\x00\x01\x00',4) },
		isJPG:     function(filepath) { return FileTypeResolver.checkMime(filepath,'image/jpeg') },
		isPDF:     function(filepath) { return FileTypeResolver.checkMime(filepath,'application/pdf') },
		isPNG:     function(filepath) { return FileTypeResolver.checkMime(filepath,'image/png') },
		isPSD:     function(filepath) { return FileTypeResolver.checkMime(filepath,'image/vnd.adobe.photoshop') },
		isSVG:     function(filepath) { return FileTypeResolver.checkBytes(filepath, /<\??(xml|svg)/,64) },
		isTGA:     function(filepath) { return FileTypeResolver.checkBytes(filepath, 'TRUEVISION',64,true) },
		isTIF:     function(filepath) { return FileTypeResolver.checkMime(filepath,'image/tiff') },

		// file compatibility with some classic file library (eg pdf manipulation, image processing)
		compatPDF: function(filepath) { return FileTypeResolver.checkBytes(filepath, '%PDF',64) },
		compatGraphikMagic: async function(filepath) {
			return false
				|| await FileTypeResolver.builtin.isICO(filepath)
				|| await FileTypeResolver.builtin.isTGA(filepath)
				|| await FileTypeResolver.builtin.isBMP(filepath)
				|| await FileTypeResolver.builtin.compatPDF(filepath)
				|| await FileTypeResolver.builtin.isAI(filepath)
				|| await FileTypeResolver.builtin.isEPS(filepath)
				|| await FileTypeResolver.builtin.isHEIC(filepath)
		},
		compatImageMagick: async function(filepath) {
			return false
				|| await FileTypeResolver.builtin.isPSD(filepath)
		},
	}
}

export const isAI      = FileTypeResolver.builtin.isAI;
export const isBMP     = FileTypeResolver.builtin.isBMP;
export const isEPS     = FileTypeResolver.builtin.isEPS;
export const isGIF     = FileTypeResolver.builtin.isGIF;
export const isHEIC    = FileTypeResolver.builtin.isHEIC;
export const isICO     = FileTypeResolver.builtin.isICO;
export const isJPG     = FileTypeResolver.builtin.isJPG;
export const isPDF     = FileTypeResolver.builtin.isPDF;
export const isPNG     = FileTypeResolver.builtin.isPNG;
export const isPSD     = FileTypeResolver.builtin.isPSD;
export const isSVG     = FileTypeResolver.builtin.isSVG;
export const isTGA     = FileTypeResolver.builtin.isTGA;
export const isTIF     = FileTypeResolver.builtin.isTIF;

export const compatPDF = FileTypeResolver.builtin.compatPDF;
export const compatGraphikMagic = FileTypeResolver.builtin.compatGraphikMagic;

export default FileTypeResolver;